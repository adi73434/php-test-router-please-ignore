<?php

namespace adi73434\TRPI;

class Router
{
	private $requestRaw;
	private $basePath;
	private $assignedRoutes = [];
	
	/**
	 * Construct the router with a basic start path.
	 * I'm probably going to do something with this to somehow allow sub-apis
	 * and all that, but that's way down the road.
	 *
	 * NOTE: For now I'm using basePath to account for hosting this on a subdirectory
	 * and the request suri giving that
	 *
	 * @param  mixed $basePath
	 * @return void
	 */
	public function __construct($basePath)
	{
		$this->basePath = $basePath;
	}



	public static function getSomething($param1)
	{
		return "Hello World";
	}



	/**
	 * Assign an available route.
	 *
	 * I wonder how bad assigning these to an array is.
	 * I was thinking of somehow memoizing the paths or something,
	 * but I don't know how that could work; I should probably research
	 * more and figure out a way to do (or an equivalent of) a switch
	 * statement based on an array.
	 *
	 * @param  mixed $route
	 * @param  mixed $runner
	 * @return void
	 */
	public function get($route, callable $runner)
	{
		// $request;
		// $response;

		array_push(
			$this->assignedRoutes,
			array(
				"route" => preg_split("/\//", $route),
				// NOTE: Runned expects a request and response
				"runner" => $runner,
			)
		);

		// $runner();
		// return array($request, $response);
	}


	private function readRequest($req)
	{
		$matchFound = false;

		$chunks = [];
		// Splti chunks by forward slash
		$chunks = preg_split("/\//", $req);

		// Remove the basePath from the request
		// $requestFormat = substr($req, strlen($this->basePath) + 1);

		// ---------------------------------------------------------------------
		// NOTE: Initial assignedRoutes idea:
		// I wanted to make each "chunk" of a route (separated by forward slashes)
		// be its own individual string within an array, therefore making each "route"
		// an array with a number of items matching the forward slashes.
		// With this, I was thinking of somehow "mapping" each "chunk" to either a runner (callback),
		// or to additional routes. So a route would be represented by an array that contains
		// additional subroutes, and each route itself would have a runner.
		// However, what this involves is quite deep nesting, and with the triple foreach, I'm already
		// starting to really not like that idea
		// ---------------------------------------------------------------------
		// TODO:
		// https://stackoverflow.com/a/21790026
		// https://stackoverflow.com/a/33560863
		// https://stackoverflow.com/a/4128377
		// - Expand the foreach implementation to match for "{var}" in routes, or ":var", or whatever,
		// so that a user can tell the library that that portion of the URI does not need to match a route
		// and that it is instead to be passed as a value to the runner.
		// - Benchmark the foreach implementation
		// - Make an implementation using regex and strings, instead of array-ing the hell out of it. This
		// would require figuring out the whole "{var}" URI assignment thing for this implementation too.
		// ---------------------------------------------------------------------
		// ---------------------------------------------------------------------

		// Try to match any request to any chunk...
		// but because assignedRoutes is an array of assoc arrays, upon which "route" is an array,
		// this clusterfuck has cometh
		foreach ($this->assignedRoutes as $reqAvailable) {
			// array_push($matched, array_intersect($chunks, $reqAvailable["route"]));
			foreach ($reqAvailable["route"] as $routeAvailable) {
				foreach ($chunks as $chunk) {
					if ($routeAvailable === $chunk) {
						$matchFound = true;
						break;
					}
				}
			}
		}

		return array(
			"matchFound" => $matchFound,
			"chunks" => $chunks,
		);
	}



	/**
	 * Route allocation is done and can start listening to the server
	 * for request info.
	 *
	 * What'd be cool is adding routes on the fly... I don't know why anyone
	 * would want that, because you can restart a server instantaneously, but
	 * nonetheless it sounded cool for at least a second in my breain.
	 *
	 * @return void
	 */
	public function run()
	{
		// ---------------------------------------------------------------------
		// TBD
		// NOTE: I want to somehow cache the "assignment" of all the get routes
		// or whatever, somehow, so that when a user hits the API it's already
		// cached as much as possible. Again, maybe memoization would be cool to
		// look into for more static-y results, but haha memory usage.
		// I really don't know what I'm doing...
		// ---------------------------------------------------------------------
		$this->requestRaw = $_SERVER["REQUEST_URI"];
		echo "Requested Route RAW: <br>";
		echo $this->requestRaw;
		echo "<br>";
		echo "<br>";

		$requestFormat = "";

		
		// Remove first slash from raw request as it makes the first array item
		// after exploding be a blank string.
		if (substr($this->requestRaw, 0, 1) === "/") {
			// Remove first forward slash, basePath, and forward slash before the first concrete route
			$requestFormat = substr($this->requestRaw, strlen($this->basePath) + 2);
			// Remove just the forward slash
			// $requestFormat = substr($this->requestRaw, 1);
		} else {
			// Remove basePath and forward slash before the first concrete route
			$requestFormat = substr($this->requestRaw, strlen($this->basePath) + 1);
			// $requestFormat = $this->requestRaw;
		}

		echo "Available Routes: <br>";
		// could maybe use route as reference here: &route;
		foreach ($this->assignedRoutes as $route) {
			echo json_encode($route["route"]);
		}


		echo "<br>";
		echo "<br>";

		echo "Requested Route: <br>";
		$reqCheck = $this->readRequest($requestFormat);
		echo json_encode($reqCheck);
	}
}
