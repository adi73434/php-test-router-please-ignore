

## NGINX
I'm building this for/with/around NGINX currently, and oh my is the config very hard coded and yucky feeling.


```nginx

location /php-router {
	# Yes, I am hosting it in WSL; I'm yet to build a homelab which will also
	# function as a dev server. This requires money, and a good job gives money :)
	alias "/mnt/d/Productivity NVMe/__Git/_PHP/php-test-router-please-ignore";

	# regex to split $uri to $fastcgi_script_name and $fastcgi_path
	fastcgi_split_path_info ^(.+?\.php)(/.*)$;

	# This was a default check within snippets to check the PHP file exists
	# before passing it... well, 99% of requests won't exist as files, because
	# they're not
	#try_files $fastcgi_script_name =404;

	# Bypass the fact that try_files resets $fastcgi_path_info
	# see: http://trac.nginx.org/nginx/ticket/321
	#set $path_info $fastcgi_path_info;
	#fastcgi_param PATH_INFO $path_info;

	#fastcgi_index index.php;
	include fastcgi.conf;

	fastcgi_param SCRIPT_FILENAME $document_root/src/index.php;
	fastcgi_pass unix:/var/run/php/php7.4-fpm.sock;
}
```